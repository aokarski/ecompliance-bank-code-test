package com.adrianokarski.codetest.ecompliance.bank.test.unit;

import com.adrianokarski.codetest.ecompliance.bank.MoneyService;
import com.adrianokarski.codetest.ecompliance.bank.MoneyServiceImpl;
import com.adrianokarski.codetest.ecompliance.bank.model.Currency;
import com.adrianokarski.codetest.ecompliance.bank.model.Money;
import com.adrianokarski.codetest.ecompliance.bank.model.exception.BankSystemIllegalStateException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class MoneyServiceTest {
    private MoneyService moneyService;

    @Before
    public void init() {
        this.moneyService = new MoneyServiceImpl();
    }

    @Test
    public void testMoneyServiceAdd() {
        BigDecimal value1 = new BigDecimal("100.44");
        BigDecimal value2 = new BigDecimal("20.11");

        Money money1 = new Money(value1, Currency.USD);
        Money money2 = new Money(value2, Currency.USD);

        Money addingResult = moneyService.addMoney(money1, money2);

        Assert.assertEquals(addingResult.getAmount(), value1.add(value2));
    }

    @Test
    public void testMoneyServiceSubtract() {
        BigDecimal value1 = new BigDecimal("100.44");
        BigDecimal value2 = new BigDecimal("20.11");

        Money money1 = new Money(value1, Currency.USD);
        Money money2 = new Money(value2, Currency.USD);

        Money addingResult = moneyService.subtractMoney(money1, money2);

        Assert.assertEquals(addingResult.getAmount(), value1.subtract(value2));
    }

    @Test(expected = BankSystemIllegalStateException.class)
    public void testMoneyServiceCurrencyMismatch() {
        BigDecimal value1 = new BigDecimal("100.44");
        BigDecimal value2 = new BigDecimal("20.11");

        Money money1 = new Money(value1, Currency.CAD);
        Money money2 = new Money(value2, Currency.USD);

        moneyService.addMoney(money1, money2);
    }
}
