package com.adrianokarski.codetest.ecompliance.bank.test.unit;

import com.adrianokarski.codetest.ecompliance.bank.*;
import com.adrianokarski.codetest.ecompliance.bank.model.BankCustomer;
import com.adrianokarski.codetest.ecompliance.bank.model.CheckingBankAccount;
import com.adrianokarski.codetest.ecompliance.bank.model.Currency;
import com.adrianokarski.codetest.ecompliance.bank.model.Money;
import com.adrianokarski.codetest.ecompliance.bank.model.exception.BankSystemIllegalStateException;
import com.adrianokarski.codetest.ecompliance.bank.model.exception.InsufficientFundsException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class CheckingAccountServiceTest {
    private static AccountService checkingAccountService;
    private static BankCustomer bankCustomer;

    @BeforeClass
    public static void init(){
        MoneyService moneyServiceMock = Mockito.mock(MoneyServiceImpl.class);
        Mockito.when(moneyServiceMock.addMoney(Mockito.any(), Mockito.any())).thenReturn(new Money(BigDecimal.ONE, Currency.CAD));
        Mockito.when(moneyServiceMock.subtractMoney(Mockito.any(), Mockito.any())).thenReturn(new Money(new BigDecimal("-1"), Currency.CAD));

        ExchangeRateService exchangeRateServiceMock = Mockito.mock(ExchangeRateServiceImpl.class);
        checkingAccountService = new CheckingAccountServiceImpl(exchangeRateServiceMock, moneyServiceMock);

        bankCustomer = Mockito.mock(BankCustomer.class);
    }

    @Test
    public void testDepositOk(){
        CheckingBankAccount bankAccount = new CheckingBankAccount(1L, bankCustomer, Currency.CAD);

        checkingAccountService.deposit(bankAccount, new Money(new BigDecimal("2"),Currency.CAD));
        Assert.assertEquals(bankAccount.getAccountBalance().getAmount(), BigDecimal.ONE);
    }

    @Test(expected = InsufficientFundsException.class)
    public void testWithdrawalError() throws InsufficientFundsException {
        CheckingBankAccount bankAccount = new CheckingBankAccount(1L, bankCustomer, Currency.CAD);
        checkingAccountService.withdrawal(bankAccount, new Money(new BigDecimal("2"),Currency.CAD));
    }

    @Test(expected = BankSystemIllegalStateException.class)
    public void testTransferError() throws InsufficientFundsException {
        CheckingBankAccount bankAccount1 = new CheckingBankAccount(1L, bankCustomer, Currency.CAD);
        CheckingBankAccount bankAccount2 = new CheckingBankAccount(2L, bankCustomer, Currency.USD);
        checkingAccountService.transfer(bankAccount1, bankAccount2, new Money(new BigDecimal("2"),Currency.CAD));
    }
}
