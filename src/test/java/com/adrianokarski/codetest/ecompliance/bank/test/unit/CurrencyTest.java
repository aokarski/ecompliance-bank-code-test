package com.adrianokarski.codetest.ecompliance.bank.test.unit;

import com.adrianokarski.codetest.ecompliance.bank.model.Currency;
import com.adrianokarski.codetest.ecompliance.bank.model.exception.BankSystemIllegalStateException;
import org.junit.Assert;
import org.junit.Test;

public class CurrencyTest {

    @Test
    public void testCurrencyCreation(){
        Currency currency = new Currency("CAD", "Canadian dollar");
        Assert.assertEquals(currency.getCode(), "CAD");
        Assert.assertEquals(currency.getDescription(), "Canadian dollar");
    }

    @Test
    public void testCurrencyEquality(){
        Currency currency1 = new Currency("CAD", "Canadian dollar");
        Currency currency2 = new Currency("CAD", " something different");
        Assert.assertEquals(currency1, currency2);
    }

    @Test
    public void testCurrencyEquality2(){
        Currency currency1 = new Currency("CAD", "Canadian dollar xxx");
        Assert.assertEquals(currency1, Currency.CAD);
    }

    @Test
    public void testCurrencyNotEquality(){
        Currency currency1 = new Currency("CADx", "Canadian dollar");
        Currency currency2 = new Currency("CAD", " something different");
        Assert.assertNotEquals(currency1, currency2);
    }

    @Test(expected = BankSystemIllegalStateException.class)
    public void testCurrencyCreation1Error1(){
        Currency currency = new Currency("CAD", "");
    }

    @Test(expected = BankSystemIllegalStateException.class)
    public void testCurrencyCreationError2(){
        Currency currency = new Currency(null, "");
    }

    @Test(expected = BankSystemIllegalStateException.class)
    public void testCurrencyCreationError3(){
        Currency currency = new Currency(null, null);
    }

    @Test(expected = BankSystemIllegalStateException.class)
    public void testCurrencyCreationError4(){
        Currency currency = new Currency("", "");
    }
}
