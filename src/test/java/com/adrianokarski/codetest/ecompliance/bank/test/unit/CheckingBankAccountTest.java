package com.adrianokarski.codetest.ecompliance.bank.test.unit;

import com.adrianokarski.codetest.ecompliance.bank.model.BankCustomer;
import com.adrianokarski.codetest.ecompliance.bank.model.CheckingBankAccount;
import com.adrianokarski.codetest.ecompliance.bank.model.Currency;
import com.adrianokarski.codetest.ecompliance.bank.model.Money;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class CheckingBankAccountTest {

    private static BankCustomer bankCustomer;

    public static void init(){
        bankCustomer = Mockito.mock(BankCustomer.class);
    }

    @Test
    public void testCheckingBankAccountCreation(){
        CheckingBankAccount checkingBankAccount = new CheckingBankAccount(1L, bankCustomer, Currency.CAD);
        Assert.assertEquals(checkingBankAccount.getBaseCurrency(), Currency.CAD);
        Assert.assertEquals(checkingBankAccount.getAccountBalance().getAmount(), BigDecimal.ZERO);
        Assert.assertEquals(checkingBankAccount.getAccountBalance().getCurrency(), checkingBankAccount.getBaseCurrency());
    }

    @Test
    public void testCheckingBankAccountSetBalanceGood(){
        final BigDecimal AMOUNT = new BigDecimal("123.33");

        CheckingBankAccount checkingBankAccount = new CheckingBankAccount(1L, bankCustomer, Currency.CAD);
        checkingBankAccount.setAccountBalance(new Money(AMOUNT, Currency.CAD));
        Assert.assertEquals(checkingBankAccount.getAccountBalance().getAmount(), AMOUNT);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckingBankAccountSetBalanceCurrencyError(){
        final BigDecimal AMOUNT = new BigDecimal("123.33");

        CheckingBankAccount checkingBankAccount = new CheckingBankAccount(1L, bankCustomer, Currency.CAD);
        checkingBankAccount.setAccountBalance(new Money(AMOUNT, Currency.USD));
    }
}
