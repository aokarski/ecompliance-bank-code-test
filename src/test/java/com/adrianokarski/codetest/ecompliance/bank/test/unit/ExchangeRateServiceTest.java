package com.adrianokarski.codetest.ecompliance.bank.test.unit;

import com.adrianokarski.codetest.ecompliance.bank.ExchangeRateProvider;
import com.adrianokarski.codetest.ecompliance.bank.ExchangeRateProviderInMemCacheImpl;
import com.adrianokarski.codetest.ecompliance.bank.ExchangeRateService;
import com.adrianokarski.codetest.ecompliance.bank.ExchangeRateServiceImpl;
import com.adrianokarski.codetest.ecompliance.bank.model.Currency;
import com.adrianokarski.codetest.ecompliance.bank.model.Money;
import com.adrianokarski.codetest.ecompliance.bank.model.exception.BankSystemIllegalStateException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Optional;

public class ExchangeRateServiceTest {
    private static ExchangeRateService exchangeRateService;

    private static final String EXAMPLE_EXCHANGE_RATE = "2";

    @BeforeClass
    public static void init(){
        ExchangeRateProvider exchangeRateProviderMock = Mockito.mock(ExchangeRateProviderInMemCacheImpl.class);
        Mockito.when(exchangeRateProviderMock.getExchangeRate(Currency.CAD, Currency.USD)).thenReturn(Optional.of(new BigDecimal(EXAMPLE_EXCHANGE_RATE)));
        Mockito.when(exchangeRateProviderMock.getExchangeRate(Currency.CAD, Currency.MXN)).thenReturn(Optional.empty());

        exchangeRateService = new ExchangeRateServiceImpl(exchangeRateProviderMock);
    }

    @Test
    public void testExchangeRateService(){
        Money money = new Money(new BigDecimal("10"), Currency.CAD);
        Money result = exchangeRateService.applyExchangeRate(money, Currency.USD);
        Assert.assertEquals(result.getAmount(), money.getAmount().multiply(new BigDecimal(EXAMPLE_EXCHANGE_RATE)));
    }

    @Test
    public void testExchangeRateServiceSameCurrency(){
        final BigDecimal JUST_TEN = BigDecimal.TEN;

        Money money = new Money(JUST_TEN, Currency.CAD);
        Money result = exchangeRateService.applyExchangeRate(money, Currency.CAD);

        Assert.assertEquals(result.getAmount(), JUST_TEN);
    }

    @Test(expected = BankSystemIllegalStateException.class)
    public void testExchangeRateServiceNoValue(){
        Money money = new Money(new BigDecimal("33"), Currency.CAD);
        Money result = exchangeRateService.applyExchangeRate(money, Currency.MXN);
    }
}
