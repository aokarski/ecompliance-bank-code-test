package com.adrianokarski.codetest.ecompliance.bank.test.functional;

import com.adrianokarski.codetest.ecompliance.bank.*;
import com.adrianokarski.codetest.ecompliance.bank.model.*;
import com.adrianokarski.codetest.ecompliance.bank.model.exception.InsufficientFundsException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class MainBusinessFunctionalTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainBusinessFunctionalTest.class);

    private static AccountService accountService;
    private static ExchangeRateService exchangeRateService;
    private static MoneyService moneyService;
    private static ExchangeRateProvider exchangeRateProvider;

    @BeforeClass
    public static void init(){
        exchangeRateProvider = new ExchangeRateProviderInMemCacheImpl();
        ((ExchangeRateProviderInMemCacheImpl)exchangeRateProvider).saveOrUpdateExchangeRate(Currency.CAD, Currency.USD, new BigDecimal("0.5"));
        ((ExchangeRateProviderInMemCacheImpl)exchangeRateProvider).saveOrUpdateExchangeRate(Currency.CAD, Currency.MXN, new BigDecimal("10"));

        exchangeRateService = new ExchangeRateServiceImpl(exchangeRateProvider);

        moneyService = new MoneyServiceImpl();

        accountService = new CheckingAccountServiceImpl(exchangeRateService, moneyService);
    }

    @Test
    public void testCaseScenario1(){
        BankCustomer customer = new BankCustomer(777L, "Stewie", "Griffin");
        BankAccount checkingBankAccount = new CheckingBankAccount(1234L, new Money(new BigDecimal("100"), Currency.CAD), customer);
        Assert.assertEquals(checkingBankAccount.getAccountBalance().getAmount(), new BigDecimal("100"));

        accountService.deposit(checkingBankAccount, new Money(new BigDecimal("300"),Currency.USD));
        Assert.assertEquals(checkingBankAccount.getAccountBalance().getAmount(), new BigDecimal("700"));

        LOGGER.info("Output1: Scenario1 account after deposit: " + checkingBankAccount);
    }

    @Test
    public void testCaseScenario2(){
        BankCustomer customer = new BankCustomer(504L, "Glen", "Quagmire");
        BankAccount checkingBankAccount = new CheckingBankAccount(2001L, new Money(new BigDecimal("35000"), Currency.CAD), customer);
        Assert.assertEquals(checkingBankAccount.getAccountBalance().getAmount(), new BigDecimal("35000"));

        boolean exception = false;

        try {
            accountService.withdrawal(checkingBankAccount, new Money(new BigDecimal("5000"), Currency.MXN));
            LOGGER.debug("Scenario2, account after first withdrawal: value:"+checkingBankAccount.getAccountBalance().getAmount());

            Assert.assertEquals(checkingBankAccount.getAccountBalance().getAmount(),
                    new BigDecimal("35000").subtract((new BigDecimal("5000").multiply(new BigDecimal("0.1")))));
        }catch (InsufficientFundsException e){
            exception = true;
        }
        Assert.assertFalse(exception);

        try {
            accountService.withdrawal(checkingBankAccount, new Money(new BigDecimal("12500"), Currency.USD));
            LOGGER.debug("Scenario2, account after second withdrawal: value:"+checkingBankAccount.getAccountBalance().getAmount());

            Assert.assertEquals(checkingBankAccount.getAccountBalance().getAmount(),
                    new BigDecimal("35000").subtract((new BigDecimal("5000").multiply(new BigDecimal("0.1"))))
                                    .subtract( (new BigDecimal("12500").multiply(new BigDecimal("2")))));
        }catch (InsufficientFundsException e){
            exception = true;
        }
        Assert.assertFalse(exception);

        accountService.deposit(checkingBankAccount, new Money(new BigDecimal("300"),Currency.CAD));
        Assert.assertEquals(checkingBankAccount.getAccountBalance().getAmount(),
                new BigDecimal("35000").subtract((new BigDecimal("5000").multiply(new BigDecimal("0.1"))))
                        .subtract( (new BigDecimal("12500").multiply(new BigDecimal("2"))))
                        .add(new BigDecimal("300")));

        LOGGER.debug("Scenario2, account after deposit: " + checkingBankAccount.getAccountBalance().getAmount());

        LOGGER.info("Output 2: Scenario2, final account state: " + checkingBankAccount);
    }

    @Test
    public void testCaseScenario3(){
        BankCustomer customer = new BankCustomer(2L, "Joe", "Swanson");
        BankAccount checkingBankAccount1 = new CheckingBankAccount(1010L, new Money(new BigDecimal("7425"), Currency.CAD), customer);
        BankAccount checkingBankAccount2 = new CheckingBankAccount(5500L, new Money(new BigDecimal("15000"), Currency.CAD), customer);

        LOGGER.debug("Scenario3, initial account1 state: " + checkingBankAccount1);
        LOGGER.debug("Scenario3, initial account2 state: " + checkingBankAccount2);

        Assert.assertEquals(checkingBankAccount1.getAccountBalance().getAmount(), new BigDecimal("7425"));
        Assert.assertEquals(checkingBankAccount2.getAccountBalance().getAmount(), new BigDecimal("15000"));

        boolean exception = false;
        try {
            accountService.withdrawal(checkingBankAccount2, new Money(new BigDecimal("5000"), Currency.CAD));
            LOGGER.debug("Scenario3, account2 after first withdrawal: value:" + checkingBankAccount2.getAccountBalance().getAmount());

            Assert.assertEquals(checkingBankAccount2.getAccountBalance().getAmount(),
                    new BigDecimal("15000").subtract((new BigDecimal("5000"))));
        }catch (InsufficientFundsException e){
            exception = true;
        }
        Assert.assertFalse(exception);

        try{
            accountService.transfer(checkingBankAccount1, checkingBankAccount2, new Money(new BigDecimal("7300"), Currency.CAD));
            LOGGER.debug("Scenario3, account1 after transfer: value:" + checkingBankAccount1.getAccountBalance().getAmount());
            LOGGER.debug("Scenario3, account2 after transfer: value:" + checkingBankAccount2.getAccountBalance().getAmount());

            Assert.assertEquals(checkingBankAccount1.getAccountBalance().getAmount(), new BigDecimal("125"));
            Assert.assertEquals(checkingBankAccount2.getAccountBalance().getAmount(), new BigDecimal("17300"));
        }catch (InsufficientFundsException e){
            exception = true;
        }
        Assert.assertFalse(exception);


        accountService.deposit(checkingBankAccount1, new Money(new BigDecimal("13726"),Currency.MXN));
        Assert.assertEquals(checkingBankAccount1.getAccountBalance().getAmount(),
                new BigDecimal("125").add((new BigDecimal("13726").multiply(new BigDecimal("0.1")))));

        LOGGER.debug("Scenario3, account1 after deposit: " + checkingBankAccount1.getAccountBalance().getAmount());

        LOGGER.info("Output 3: Scenario3, final account1 state: " + checkingBankAccount1);
        LOGGER.info("Output 3: Scenario3, final account2 state: " + checkingBankAccount2);
    }

    @Test
    public void testCaseScenario4(){
        BankCustomer customer1 = new BankCustomer(123L, "Pwter", "Griffin");
        BankCustomer customer2 = new BankCustomer(456L, "Louis", "Griffin");
        BankAccount checkingBankAccount1 = new CheckingBankAccount(123L, new Money(new BigDecimal("150"), Currency.CAD), customer1);
        BankAccount checkingBankAccount2 = new CheckingBankAccount(456L, new Money(new BigDecimal("65000"), Currency.CAD), customer2);

        LOGGER.debug("Scenario4, initial account1 state: " + checkingBankAccount1);
        LOGGER.debug("Scenario4, initial account2 state: " + checkingBankAccount2);

        boolean exception = false;
        try {
            accountService.withdrawal(checkingBankAccount1, new Money(new BigDecimal("70"), Currency.USD));
            LOGGER.debug("Scenario4, account1 after first withdrawal: value:" + checkingBankAccount1.getAccountBalance().getAmount());

            Assert.assertEquals(checkingBankAccount1.getAccountBalance().getAmount(), new BigDecimal("10"));
        }catch (InsufficientFundsException e){
            exception = true;
        }
        Assert.assertFalse(exception);

        accountService.deposit(checkingBankAccount2, new Money(new BigDecimal("23789"),Currency.USD));
        Assert.assertEquals(checkingBankAccount2.getAccountBalance().getAmount(),
                new BigDecimal("65000").add((new BigDecimal("23789").multiply(new BigDecimal("2")))));

        LOGGER.debug("Scenario3, account2 after deposit: " + checkingBankAccount2.getAccountBalance().getAmount());

        try{
            accountService.transfer(checkingBankAccount2, checkingBankAccount1, new Money(new BigDecimal("23.75"), Currency.CAD));
            LOGGER.debug("Scenario4, account1 after transfer: value:" + checkingBankAccount1.getAccountBalance().getAmount());
            LOGGER.debug("Scenario4, account2 after transfer: value:" + checkingBankAccount2.getAccountBalance().getAmount());

            Assert.assertEquals(checkingBankAccount1.getAccountBalance().getAmount(), new BigDecimal("33.75"));
            Assert.assertEquals(checkingBankAccount2.getAccountBalance().getAmount(), new BigDecimal("112554.25"));
        }catch (InsufficientFundsException e){
            exception = true;
        }
        Assert.assertFalse(exception);

        LOGGER.info("Output 4: Scenario4, final account1 state: " + checkingBankAccount1);
        LOGGER.info("Output 4: Scenario4, final account2 state: " + checkingBankAccount2);
    }
}
