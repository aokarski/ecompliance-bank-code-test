package com.adrianokarski.codetest.ecompliance.bank.test.unit;

import com.adrianokarski.codetest.ecompliance.bank.ExchangeRateProvider;
import com.adrianokarski.codetest.ecompliance.bank.ExchangeRateProviderInMemCacheImpl;
import com.adrianokarski.codetest.ecompliance.bank.model.Currency;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.NoSuchElementException;

public class ExchangeRateProviderTest {
    private ExchangeRateProvider exchangeRateProvider;

    @Before
    public void init(){
        exchangeRateProvider = new ExchangeRateProviderInMemCacheImpl();

        Currency cad = new Currency("CAD", "canada dollar");
        Currency usd = new Currency("USD", "us dollar");
        Currency mex = new Currency("MEX", "mexico pesso");

        ((ExchangeRateProviderInMemCacheImpl)exchangeRateProvider).saveOrUpdateExchangeRate(cad, usd, new BigDecimal("0.5"));
        ((ExchangeRateProviderInMemCacheImpl)exchangeRateProvider).saveOrUpdateExchangeRate(cad, mex, new BigDecimal("10"));
    }

    @Test
    public void exchangeRateReadTest1(){
        Currency cad = new Currency("CAD", "canada dollar");
        Currency usd = new Currency("USD", "us dollar");

        Assert.assertEquals(exchangeRateProvider.getExchangeRate(cad, usd).get(), new BigDecimal("0.5"));
    }

    @Test
    public void exchangeRateReadTest1Reveresed(){
        Currency cad = new Currency("CAD", "canada dollar");
        Currency usd = new Currency("USD", "us dollar");

        Assert.assertEquals(exchangeRateProvider.getExchangeRate(usd, cad).get(), new BigDecimal("2"));
    }

    @Test(expected = NoSuchElementException.class)
    public void exchangeRateReadNotPresent(){
        Currency cad = new Currency("CAD", "canada dollar");
        Currency xxx = new Currency("XXX", "xxx");
        exchangeRateProvider.getExchangeRate(cad, xxx).get();
    }

    @Test
    public void exchangeRateReadTestIdempotent(){
        Currency cad = new Currency("CAD", "canada dollar");
        Currency usd = new Currency("USD", "us dollar");

        Assert.assertEquals(exchangeRateProvider.getExchangeRate(cad, usd).get(), new BigDecimal("0.5"));
        Assert.assertEquals(exchangeRateProvider.getExchangeRate(usd, cad).get(), new BigDecimal("2"));

        ((ExchangeRateProviderInMemCacheImpl)exchangeRateProvider).saveOrUpdateExchangeRate(cad, usd, new BigDecimal("0.5"));

        Assert.assertEquals(exchangeRateProvider.getExchangeRate(cad, usd).get(), new BigDecimal("0.5"));
        Assert.assertEquals(exchangeRateProvider.getExchangeRate(usd, cad).get(), new BigDecimal("2"));
    }
}
