package com.adrianokarski.codetest.ecompliance.bank.test.unit;

import com.adrianokarski.codetest.ecompliance.bank.model.Currency;
import com.adrianokarski.codetest.ecompliance.bank.model.Money;
import com.adrianokarski.codetest.ecompliance.bank.model.exception.BankSystemIllegalStateException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class MoneyTest {
    @Test
    public void testMoneyCreation(){
        Money money = new Money(new BigDecimal("123.23"), Currency.USD);
        Assert.assertEquals(money.getAmount(), new BigDecimal("123.23"));
        Assert.assertEquals(money.getCurrency(), new Currency("USD", "description"));
    }

    @Test
    public void testMoneyEquality(){
        Money moneyUsd1 = new Money(new BigDecimal("123.23"), Currency.USD);
        Money moneyUsd2 = new Money(new BigDecimal("123.23"), Currency.USD);

        Assert.assertEquals(moneyUsd1, moneyUsd2);
    }

    @Test
    public void testMoneyNotEquality(){
        Money moneyUsd1 = new Money(new BigDecimal("123.23"), Currency.USD);
        Money moneyCad = new Money(new BigDecimal("123.23"), Currency.CAD);

        Assert.assertNotEquals(moneyUsd1, moneyCad);
    }

    @Test(expected = BankSystemIllegalStateException.class)
    public void testMoneyCreationError1(){
        Money money = new Money(BigDecimal.ZERO, null);
    }

    @Test(expected = BankSystemIllegalStateException.class)
    public void testMoneyCreationError2(){
        Money money = new Money(null, null);
    }
}
