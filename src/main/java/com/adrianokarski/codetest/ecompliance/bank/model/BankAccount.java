package com.adrianokarski.codetest.ecompliance.bank.model;

public abstract class BankAccount {
    protected Long id;
    protected Money accountBalance;

    protected BankCustomer bankCustomer;

    public BankAccount(Long id, Money accountBalance, BankCustomer bankCustomer) {
        this.id = id;
        this.accountBalance = accountBalance;
        this.bankCustomer = bankCustomer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Money getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Money accountBalance) {
        this.accountBalance = accountBalance;
    }

    public BankCustomer getBankCustomer() {
        return bankCustomer;
    }

    public void setBankCustomer(BankCustomer bankCustomer) {
        this.bankCustomer = bankCustomer;
    }
}
