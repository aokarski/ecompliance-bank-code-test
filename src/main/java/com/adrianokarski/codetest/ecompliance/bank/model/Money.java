package com.adrianokarski.codetest.ecompliance.bank.model;

import com.adrianokarski.codetest.ecompliance.bank.model.exception.BankSystemIllegalStateException;

import java.math.BigDecimal;

public final class Money {
    private final BigDecimal amount;
    private final Currency currency;

    public Money(BigDecimal amount, Currency currency) {
        if(amount == null || currency == null){
            throw new BankSystemIllegalStateException("invalid data creating money");
        }
        this.amount = amount;
        this.currency = currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Money)) return false;

        Money money = (Money) o;

        if (getAmount() != null ? !getAmount().equals(money.getAmount()) : money.getAmount() != null) return false;
        return getCurrency() != null ? getCurrency().equals(money.getCurrency()) : money.getCurrency() == null;
    }

    @Override
    public int hashCode() {
        int result = getAmount() != null ? getAmount().hashCode() : 0;
        result = 31 * result + (getCurrency() != null ? getCurrency().hashCode() : 0);
        return result;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "Money{" +
                "amount=" + amount +
                ", currency=" + currency +
                '}';
    }
}
