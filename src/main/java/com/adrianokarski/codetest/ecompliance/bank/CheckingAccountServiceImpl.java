package com.adrianokarski.codetest.ecompliance.bank;

import com.adrianokarski.codetest.ecompliance.bank.model.BankAccount;
import com.adrianokarski.codetest.ecompliance.bank.model.CheckingBankAccount;
import com.adrianokarski.codetest.ecompliance.bank.model.Money;
import com.adrianokarski.codetest.ecompliance.bank.model.exception.BankSystemIllegalStateException;
import com.adrianokarski.codetest.ecompliance.bank.model.exception.InsufficientFundsException;

import java.math.BigDecimal;

public class CheckingAccountServiceImpl implements AccountService{
    private final ExchangeRateService exchangeRateService;
    private final MoneyService moneyService;

    public CheckingAccountServiceImpl(ExchangeRateService exchangeRateService, MoneyService moneyService) {
        this.exchangeRateService = exchangeRateService;
        this.moneyService = moneyService;
    }

    public void deposit(BankAccount targetAccount, Money money){
        checkInputData(targetAccount);
        CheckingBankAccount targetCheckingBankAccount = (CheckingBankAccount) targetAccount;

        Money afterExchangeRate = exchangeRateService.applyExchangeRate(money, targetCheckingBankAccount.getBaseCurrency());

        Money afterDeposit = moneyService.addMoney(targetCheckingBankAccount.getAccountBalance(), afterExchangeRate);
        targetCheckingBankAccount.setAccountBalance(afterDeposit);
    }

    public void withdrawal(BankAccount targetAccount, Money money) throws InsufficientFundsException {
        checkInputData(targetAccount);
        CheckingBankAccount targetCheckingBankAccount = (CheckingBankAccount) targetAccount;

        Money afterExchangeRate = exchangeRateService.applyExchangeRate(money, targetCheckingBankAccount.getBaseCurrency());

        //enough money to do the withdrawal?
        Money afterWithdrawal = checkAndCalculateAfterWithdrawal(targetCheckingBankAccount, afterExchangeRate);

        targetCheckingBankAccount.setAccountBalance(afterWithdrawal);
    }

    public void transfer(BankAccount srcAccount, BankAccount targetAccount, Money money) throws InsufficientFundsException{
        checkInputData(srcAccount);
        checkInputData(targetAccount);
        CheckingBankAccount srcCheckingBankAccount = (CheckingBankAccount) srcAccount;
        CheckingBankAccount targetCheckingBankAccount = (CheckingBankAccount) targetAccount;

        if(!srcCheckingBankAccount.getBaseCurrency().equals(targetCheckingBankAccount.getBaseCurrency())){
            throw new BankSystemIllegalStateException("bank accounts currencies mismatch");
        }

        Money afterExchangeRate = exchangeRateService.applyExchangeRate(money, targetCheckingBankAccount.getBaseCurrency());

        // enough money on the src account?
        Money afterWithdrawal = checkAndCalculateAfterWithdrawal(srcCheckingBankAccount, afterExchangeRate);
        Money afterDeposit = moneyService.addMoney(targetCheckingBankAccount.getAccountBalance(), afterExchangeRate);

        srcCheckingBankAccount.setAccountBalance(afterWithdrawal);
        targetCheckingBankAccount.setAccountBalance(afterDeposit);

    }

    private Money checkAndCalculateAfterWithdrawal(CheckingBankAccount targetAccount, Money money) throws InsufficientFundsException {
        Money afterWithdrawal = moneyService.subtractMoney(targetAccount.getAccountBalance(), money);
        if(afterWithdrawal.getAmount().compareTo(BigDecimal.ZERO) < 0){
            throw new InsufficientFundsException();
        }

        return afterWithdrawal;
    }

    private void checkInputData(BankAccount account){
        if(!(account instanceof CheckingBankAccount)){
            throw new IllegalArgumentException("CheckingBankAccount object expected");
        }
    }
}
