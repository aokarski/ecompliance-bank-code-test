package com.adrianokarski.codetest.ecompliance.bank;

import com.adrianokarski.codetest.ecompliance.bank.model.Currency;
import com.adrianokarski.codetest.ecompliance.bank.model.Money;

public interface ExchangeRateService {
    Money applyExchangeRate(final Money src, final Currency targetCurrency);
}
