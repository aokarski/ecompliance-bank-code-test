package com.adrianokarski.codetest.ecompliance.bank;

import com.adrianokarski.codetest.ecompliance.bank.model.BankAccount;
import com.adrianokarski.codetest.ecompliance.bank.model.CheckingBankAccount;
import com.adrianokarski.codetest.ecompliance.bank.model.Money;
import com.adrianokarski.codetest.ecompliance.bank.model.exception.InsufficientFundsException;

public interface AccountService {
    void deposit(BankAccount targetAccount, Money money);
    void withdrawal(BankAccount targetAccount, Money money) throws InsufficientFundsException;
    void transfer(BankAccount srcAccount, BankAccount targetAccount, Money money) throws InsufficientFundsException;
}
