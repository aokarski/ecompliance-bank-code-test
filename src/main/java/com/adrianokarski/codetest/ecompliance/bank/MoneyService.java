package com.adrianokarski.codetest.ecompliance.bank;

import com.adrianokarski.codetest.ecompliance.bank.model.Money;

public interface MoneyService {
    Money addMoney(Money arg1, Money arg2);
    Money subtractMoney(Money arg1, Money arg2);
}
