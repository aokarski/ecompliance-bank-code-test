package com.adrianokarski.codetest.ecompliance.bank;

import com.adrianokarski.codetest.ecompliance.bank.model.Currency;

import java.math.BigDecimal;
import java.util.Optional;

public interface ExchangeRateProvider {
    Optional<BigDecimal> getExchangeRate(final Currency from, final Currency to);
}
