package com.adrianokarski.codetest.ecompliance.bank;

import com.adrianokarski.codetest.ecompliance.bank.model.exception.BankSystemIllegalStateException;
import com.adrianokarski.codetest.ecompliance.bank.model.Currency;
import com.adrianokarski.codetest.ecompliance.bank.model.Money;

import java.math.BigDecimal;

public class ExchangeRateServiceImpl implements ExchangeRateService{
    private final ExchangeRateProvider exchangeRateProvider;

    public ExchangeRateServiceImpl(ExchangeRateProvider exchangeRateProvider) {
        this.exchangeRateProvider = exchangeRateProvider;
    }

    public Money applyExchangeRate(final Money src, final Currency targetCurrency){
        // the same currency:
        if(src.getCurrency().equals(targetCurrency)) return src;

        BigDecimal exchangeRate = exchangeRateProvider.getExchangeRate(src.getCurrency(), targetCurrency)
                .orElseThrow(() -> new BankSystemIllegalStateException("No exchange rate defined: from " + src.getCurrency().getCode() + " to " + targetCurrency.getCode()));

        return new Money(src.getAmount().multiply(exchangeRate), targetCurrency);
    }
}
