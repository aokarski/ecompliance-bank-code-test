package com.adrianokarski.codetest.ecompliance.bank.model;

import com.adrianokarski.codetest.ecompliance.bank.model.exception.BankSystemIllegalStateException;

public final class Currency {
    public static final Currency CAD = new Currency("CAD", "Canada dollar");
    public static final Currency USD = new Currency("USD", "US dollar");
    public static final Currency MXN = new Currency("MXN", "Mexico peso");

    private final String code;
    private final String description;

    public Currency(String code, String description) {
        if(code == null || code.isEmpty() || description == null || description.isEmpty()){
            throw new BankSystemIllegalStateException("creating invalid currency");
        }

        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Currency)) return false;

        Currency currency = (Currency) o;

        return getCode().equals(currency.getCode());
    }

    @Override
    public int hashCode() {
        return getCode().hashCode();
    }

    @Override
    public String toString() {
        return "Currency{" +
                "code='" + code + '\'' +
                '}';
    }
}
