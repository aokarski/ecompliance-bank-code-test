package com.adrianokarski.codetest.ecompliance.bank.model.exception;

public class BankSystemIllegalStateException extends RuntimeException {
    public BankSystemIllegalStateException() {
    }

    public BankSystemIllegalStateException(String message) {
        super(message);
    }

    public BankSystemIllegalStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public BankSystemIllegalStateException(Throwable cause) {
        super(cause);
    }

    public BankSystemIllegalStateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
