package com.adrianokarski.codetest.ecompliance.bank;

import com.adrianokarski.codetest.ecompliance.bank.model.Money;
import com.adrianokarski.codetest.ecompliance.bank.model.exception.BankSystemIllegalStateException;

public class MoneyServiceImpl implements MoneyService{
    public Money addMoney(Money arg1, Money arg2){
        checkCurrencyMatch(arg1, arg2);

        return new Money(arg1.getAmount().add(arg2.getAmount()), arg1.getCurrency());
    }

    public Money subtractMoney(Money arg1, Money arg2){
        checkCurrencyMatch(arg1, arg2);

        return new Money(arg1.getAmount().subtract(arg2.getAmount()), arg1.getCurrency());
    }

    private void checkCurrencyMatch(Money arg1, Money arg2){
        if(!areTheSameCurrency(arg1, arg2)){
            throw new BankSystemIllegalStateException("invalid money operation");
        }
    }

    private boolean areTheSameCurrency(Money arg1, Money arg2){
        return arg1.getCurrency().equals(arg2.getCurrency());
    }
}
