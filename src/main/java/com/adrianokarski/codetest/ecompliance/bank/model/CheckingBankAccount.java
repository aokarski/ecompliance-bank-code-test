package com.adrianokarski.codetest.ecompliance.bank.model;

import java.math.BigDecimal;

public class CheckingBankAccount extends BankAccount {
    private final Currency baseCurrency;

    public void setAccountBalance(Money accountBalance) {
        if(!accountBalance.getCurrency().equals(baseCurrency)){
            throw new IllegalArgumentException("can't change base account's currency");
        }
        this.accountBalance = accountBalance;
    }

    public CheckingBankAccount(Long id, Money accountBalance, BankCustomer bankCustomer) {
        super(id, accountBalance, bankCustomer);
        this.baseCurrency = accountBalance.getCurrency();
    }

    public CheckingBankAccount(Long id, BankCustomer bankCustomer, Currency baseCurrency) {
        super(id, new Money(BigDecimal.ZERO, baseCurrency), bankCustomer);
        this.baseCurrency = baseCurrency;
    }

    public Currency getBaseCurrency() {
        return baseCurrency;
    }

    @Override
    public String toString() {
        return "CheckingBankAccount{" +
                "baseCurrency=" + baseCurrency +
                ", id=" + id +
                ", accountBalance=" + accountBalance +
                ", bankCustomer=" + bankCustomer +
                '}';
    }
}
