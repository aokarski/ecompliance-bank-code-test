package com.adrianokarski.codetest.ecompliance.bank;

import com.adrianokarski.codetest.ecompliance.bank.model.Currency;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ExchangeRateProviderInMemCacheImpl implements ExchangeRateProvider{

    private Map<Currency, Map<Currency, BigDecimal>> exchangeRateStorage = new HashMap<>();

    public void saveOrUpdateExchangeRate(final Currency from, final Currency to,final BigDecimal value){
        if(from == null | to == null || value == null || value.equals(BigDecimal.ZERO)){
            throw new IllegalArgumentException("invalid exchange rate data");
        }

        storeOrUpdateExchangeRate(from, to, value);

        // we want to store exchangeRate the other way as well
        BigDecimal reversedValue = BigDecimal.ONE.divide(value);
        storeOrUpdateExchangeRate(to, from, reversedValue);

    }

    private void storeOrUpdateExchangeRate(Currency from, Currency to, BigDecimal value){
        if(!exchangeRateStorage.containsKey(from)){
            exchangeRateStorage.put(from, new HashMap<>());
        }

        exchangeRateStorage.get(from).put(to, value);
    }

    @Override
    public Optional<BigDecimal> getExchangeRate(final Currency from, final Currency to) {
        if(from.equals(to)) return Optional.of(BigDecimal.ONE);

        if(!exchangeRateStorage.containsKey(from)) return Optional.empty();

        return Optional.ofNullable(exchangeRateStorage.get(from).get(to));
    }
}
